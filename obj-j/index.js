var fs = require ("fs");
fs.readFile("./objc.lang", "utf-8", function(error, content){
    let lexem_table_text = fs.readFileSync("lexTable.json", "utf8");
    let lexem_table = JSON.parse(lexem_table_text);
    //let match = RegExp("\/\\*.*\n*\\*\/").exec(text);
    let lexems = lexer(content, lexem_table);
    console.log(lexems);
})


function lexer(code, table){
    var lexemes = [];
    var constArray = [];
    var indentArray = [];

    while(code.length>0)//пока остались входные
    {   
        var flag = false; //флаг наличия лексемы
        for(var key in table)
        {
            //Берем регулярку из таблицы и проверяем на совпадение в коде
            var reg = new RegExp(table[key].regex)
            var match = reg.exec(code);
            if(flag)
                flag=false;
            //Если совпадение есть и совпадение не равно 0
            if(match&&match.length)
            {   
                //Проверка на необходимость пропуска
                if(table[key].skip){
                    code=code.replace(reg, "");
                    flag = true;
                    //break;
                }
                //Если пропуск не нужен, то проверяем наличие списка лексем по данной регулярке
                else if(table[key].list)
                {         
                    for(var first in table[key].list)
                         for(var second in table[key].list[first])
                            if(match==table[key].list[first][second]){
                                lexemes.push([key, first, table[key].list[first][second]]);
                                code=code.replace(reg, "");
                                flag=true;
                                break;
                        }
                }
                //Если нет списка, то скорее всего это константная строка или индентификатор
                else{

                    //Проверяем наличие константы в массиве констант
                    for(var i =0; i < constArray.length; i++)
                    {
                        if(constArray[i][1]== match[0])
                        { 
                            lexemes.push([table[key].link, i]); 
                            flag = true;
                        }
                    }

                    //Проверяем наличие индентификаторов в массиве понятно каком
                    for(var i = 0; i<indentArray.length; i++)
                    {
                        if(indentArray[i][1]==match[0])
                        {
                            lexemes.push([table[key].link, i]);
                            flag = true;
                        }
                    }
                    //Если не нашли лексему в массиве, добавляем
                        if(!flag&&table[key].link == "arrayOfConst")
                    {
                        constArray.push([constArray.length, match[0]]); 
                        lexemes.push([table[key].link, constArray.length-1]); 
                        flag = true;
                    }
                        if(!flag&&table[key].link == "arrayOfIdent")
                    {
                        indentArray.push([indentArray.length, match[0]]); 
                        lexemes.push([table[key].link, indentArray.length-1]); 
                        flag = true;
                    } 
                    
                    code = code.replace(reg, "");
                 }
            }
        }
    }
    console.log("Array of const:", constArray);
    console.log("Array of ident:", indentArray);
    return lexemes;
}

