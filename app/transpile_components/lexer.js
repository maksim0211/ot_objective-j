(function(trans) {

  /**
   * Это лексический анализатор. Здесь должен быть ваш код вместо данной затычки!
   * @param {string} inputCode - строка с кодом программы на входном языке
   * @return {Array} - массив распознанных лексем
   */
  trans.TransProto.lexer = function (inputCode) {
    // Это всего лишь пример, как можно разбить на лексемы. Разумеется полный лексер значительно сложнее.
    this.lexArray = inputCode.split(' ');
    return this.lexArray;
  }

})(window.trans || (window.trans = {}));